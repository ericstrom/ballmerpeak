<?php
/*
* Block Name: Hero
* Block Namespace: headings
* Block Identifier: hero
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: Simple image wrapped in a div element
*/
?>
<div class="hero {{name:additional-classes; type:text; label:Additional Classes; position:4}}">
    <div class="background" style="background:url({{name:image; type:image; label:Image; position:1}}) center bottom; background-size: cover;"></div>
    <div class="tint"></div>
     <div class="row">
         <div class="content">
                <h1>{{name:headline; type:text; label:Headline; position:2}}</h1>
                <a class="button hollow" href="{{name:link; type:text; label:Link; position:5}}">
                    {{name:linktext; type:text; label:Link Text; position:4}}
                </a>
         </div>
         <div class="logo">
            <img src="/wp-content/themes/ballmerpeak/images/logo-desktop.svg" />
         </div>
     </div>
</div>
