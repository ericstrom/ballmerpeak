<?php
/*
* Block Name: Heading
* Block Namespace: basic
* Block Identifier: heading
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: H2 tag with site wide heading styles
*/
?>

<h2 class="heading {{name:spacing; type:spacing; label:Spacing; position:4}}
    {{name:classes; type:text; label:Additional Classes; position:3}}"
    styles="{{name:styles; type:text; label:Additional Styles; position:2}}">
    {{name:heading; type:text; label:Heading; position:1}}
</h2>
