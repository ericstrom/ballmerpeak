<?php
/*
* Block Name: Html
* Block Namespace: basic
* Block Identifier: html
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: Raw HTML input. There is no wrapper around this particular element, only what you enter will render on the page.
*/
?>

{{name:html; type:content; label:Raw HTML}}
