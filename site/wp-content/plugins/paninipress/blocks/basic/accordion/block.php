<?php
/*
* Block Name: Accordion
* Block Namespace: basic
* Block Identifier: accordion
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: Expandable accordion with heading and content area
*/
?>
<?php
$name = "checkbox-" . rand(0, 9999);
?>
<div class="accordion {{name:spacing; type:spacing; label:Spacing; position:5}}
    {{name:classes; type:text; label:Additional Classes; position:3}}"
    style="{{name:styles; type:text; label:Additional Styles; position:4}}">
    <input type="checkbox" id="<?php echo $name; ?>" class="accordion-checkbox" />
    <h3><label for="<?php echo $name; ?>">{{name:heading; type:text; label:Heading; position:1}}</label></h3>
    <div class="content">
        {{name:content; label:Content; position:2; type:content}}
    </div>
</div>
