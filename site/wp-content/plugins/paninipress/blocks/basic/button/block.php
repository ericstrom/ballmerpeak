<?php
/*
* Block Name: Button
* Block Namespace: basic
* Block Identifier: button
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: Simple button with 3 styles
*/
?>

<a class="button {{name:classes; type:text; label:Additional Classes; position:3}}
   {{name:spacing; type:spacing; label:Button Spacing; position:4}}"
    href="{{name:link; type:text; label:Link; position:2}}">
    {{name:text; type:text; label:Button Text; position:1}}
</a>
