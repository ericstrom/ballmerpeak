<?php
/*
* Block Name: Image
* Block Namespace: basic
* Block Identifier: image
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: Simple image wrapped in a div element
*/
?>
<div class="image-wrapper {{name:additional-classes; type:text; position:4}}" 
     style="{{name:additional-styles; type:text; position:3}}">
    <img src="{{name:image; type:image; position:1}}" 
         alt="{{name:alt-text; type:text; position:2}}" />
</div>
