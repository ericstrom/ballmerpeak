<?php
/*
* Block Name: Paragraph
* Block Namespace: basic
* Block Identifier: paragraph
* Block Dependencies:
* Block Includes:
* Block Classes:
* Block Description: This is basic paragraph text. DO NOT include opening and closing tags. Use multiple blocks for multiple paragraphs.
*/
?>

<p class="{{name:additional-classes; label:Additional Classes; type:text; position:2}}
          {{name:spacing; label:Paragraph Spacing; type:spacing; position:4}}" 
   style="{{name:additional-styles; label:Additional Styles; type:text; position:1}}">
   {{name:content; type:content; label:Paragraph Content; position:3}}</p>
