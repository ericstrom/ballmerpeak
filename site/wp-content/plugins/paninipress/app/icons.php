<?php
/*
* Logic for custom post type icons
 */

// create custom post type
function paniniCreateIconPostType() {
    register_post_type('icon',
        array(
            'labels' => array(
                'name' => __('Icons'),
                'singular_name' => __('Icon'),
                'add_new_item'  => 'Add New Icon',
                'featured_image'    => 'Icon Image',
                'set_featured_image'    => 'Set Icon Image'
            ),
            'public' => true,
            'exclude_from_search' => true,
            'rewrite' => array('slug' => 'icon'),
            'publicly_queryable'    => false,
            'show_in_menu' => false,
            'supports'  => array('title', 'thumbnail')
        )
    );
}
add_action('init', 'paniniCreateIconPostType');

function addIconMenuItem() {
    $iconLink = 'edit.php?post_type=icon';
    add_submenu_page('paninipress', 'Icons', 'Icons', 'manage_options', $iconLink);
}

add_action("admin_menu", "addIconMenuItem");

// move meta box
function paniniIconMoveMeta(){
    remove_meta_box( 'postimagediv', 'icon', 'side' );
    add_meta_box('postimagediv', __('Icon Image'), 'post_thumbnail_meta_box', 'icon', 'normal', 'high');
}
add_action('do_meta_boxes', 'paniniIconMoveMeta');

/*
* Building the styles and actions
*/

function paniniCheckLibrary() {
    // check imagick
    if (extension_loaded('imagick') && class_exists('Imagick', false) && class_exists('ImagickPixel', false)) {
        return "imagick";
    }
    // check gd
    if (extension_loaded('gd') && function_exists('gd_info')) {
        return "gd";
    }
    // if neither of these, return false
    return false;
}

function paniniBuildIcons() {
    // check which method we'll be using
    $library = paniniCheckLibrary();
    if (!$library) {
        return false;
    }
    // get all posts
    $args = array(
        'post_type'         => 'icon',
        'posts_per_page'    => -1
    );
    $icons = get_posts($args);
    // we need this to convert the URL to the file path
    $uploadDir = wp_upload_dir();
    // get all file paths, put them in to an array
    $iconFiles = array();
    $finalImageWidth = 0;
    $finalImageHeight = 0;
    foreach ($icons as $icon) {
        $fileUrl = get_the_post_thumbnail_url($icon->ID, "full");
        $filePath = str_replace($uploadDir["baseurl"], $uploadDir["basedir"], $fileUrl);
        $imageSize = getimagesize($filePath);
        $imageSize["post_name"] = $icon->post_name;
        $iconFiles[$filePath] = $imageSize;
        // go through and set what our final sprite size will be
        $finalImageWidth += $imageSize[0];
        if ($imageSize[1] > $finalImageHeight) {
            $finalImageHeight = $imageSize[1];
        }
    }

    // start processing the stylesheet
    if ($library == "imagick") {

    } else if ($library == "gd") {
        // create image that's the size of our final width and height
        $baseImage = imagecreatetruecolor($finalImageWidth, $finalImageHeight);
        $black = imagecolorallocate($baseImage, 0, 0, 0);
        imagecolortransparent($baseImage, $black);
        imagealphablending($baseImage, false);
        imagesavealpha($baseImage, true);
        // next two lines set the image background to transparent
        // loop through each image and add it to the sprite and generate css
        $xPosition = 0;
        $cssString = ".panini-icon {background: url(../images/icon-sprite.png); display:inline-block;}\n";
        foreach ($iconFiles as $filePath => $fileInfo) {
            // sprite generation
            $mergingImage = imagecreatefrompng($filePath);
            imagealphablending($mergingImage, true);
            imagesavealpha($mergingImage, true);
            // merget it in to the base image
            imagecopy($baseImage, $mergingImage, $xPosition, 0, 0, 0, $fileInfo[0], $fileInfo[1]);

            // CSS Generation
            $cssString .= ".panini-icon." . $fileInfo["post_name"] . ";";
            $cssString .= "{background-position:-" . $xPosition . "px 0;"; 
            $cssString .= "height:" . $fileInfo[1] . "px;";
            $cssString .= "width:" . $fileInfo[0] . "px;";
            $cssString .= "}\n";

            // set new x coordinate for the next image
            $xPosition += $fileInfo[0];
        }
        //output the image
        $outputImagePath = plugin_dir_path(__DIR__) . "frontend/images/icon-sprite.png";
        imagepng($baseImage, $outputImagePath);
        // output the css
        $outputImagePath = plugin_dir_path(__DIR__) . "frontend/css/icons.css";
        file_put_contents($outputImagePath, $cssString);
    }
    // there should only be two options,
    // one of these should run
    return false;
}
add_action("wp_ajax_rebuildicons", "paniniBuildIcons");
add_action("save_post_icon", "paniniBuildIcons", 13, 2);