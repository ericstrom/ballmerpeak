<?php
/*
* Logic for custom post type icons
 */

// create custom post type
function paniniCreateSlidePostType() {
    register_post_type('paninislide',
        array(
            'labels' => array(
                'name' => __('Sliders'),
                'singular_name' => __('Slider'),
                'add_new_item'  => 'Add New Slide',
                'featured_image'    => 'Slide Image',
                'set_featured_image'    => 'Set Slide Image'
            ),
            'public' => true,
            'exclude_from_search' => true,
            'rewrite' => array('slug' => 'icon'),
            'publicly_queryable'    => false,
            'show_in_menu' => false,
            'supports'  => array('title', 'thumbnail')
        )
    );
}
add_action('init', 'paniniCreateSlidePostType');

function createPaniniSlidesTax() {
    register_taxonomy(
        'paninislider',
        'paninislide',
        array(
            'label' => __( 'Sliders' ),
            'rewrite' => array( 'slug' => 'sliders' ),
            'hierarchical' => true,
        )
    );
}

add_action( 'init', 'createPaniniSlidesTax' );

function addSlidesMenuItem() {
    add_submenu_page('paninipress', 'Sliders', 'Sliders', 'manage_options', "sliders", "sliderPageCallback");
}

add_action("admin_menu", "addSlidesMenuItem");


function sliderPageCallback() {
    include("slides/template/admin.php");
}