<?php
/*
Plugin Name: Panini Press
Plugin URI: http://paninipresswp.com
Description: Visual page builder meant for both developers and users
Version: 0.1
Author: Eric Strom
Author URI: http://paninipresswp.com
License: DBAD https://www.dbad-license.org/
*/

/*
* This shouldn't even be a thing, let's remove it
*/
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

// add global ajax objects
function paniniJsVariables(){ ?>
      <script type="text/javascript">
        var panini_ajax_object = {
            "ajax_url": '<?php echo admin_url( "admin-ajax.php" ); ?>'
        };
        var ajaxnonce = '<?php echo wp_create_nonce( "itr_ajax_nonce" ); ?>';
      </script><?php
}

add_action ('admin_head', 'paniniJsVariables');

/*
* Admin scripts and styles
*/

function paniniAdminScripts() {
    // lib
    wp_enqueue_script("jquery-ui-core");
    wp_enqueue_script("jquery-ui-sortable");
    // plugin
    wp_enqueue_script("panini-admin-scripts", plugin_dir_url(__FILE__) . "/js/admin.js", array("jquery"));
}
add_action("admin_enqueue_scripts", "paniniAdminScripts");

function paniniAdminStyles() {
    // lib
    wp_enqueue_style("font-awesome", plugin_dir_url(__FILE__) . "/css/lib/font-awesome.min.css");
    // local
    wp_enqueue_style("panini-admin-styles", plugin_dir_url(__FILE__) . "/css/admin.css");
}
add_action("admin_enqueue_scripts", "paniniAdminStyles");

/*
* Front end scripts and styles
*/

function paniniFrontEndStyles() {
    // lib
    wp_enqueue_style("slick-styles", plugin_dir_url(__FILE__) . "/js/lib/slick/slick.css");
    wp_enqueue_style("slick-theme", plugin_dir_url(__FILE__) . "/js/lib/slick/slick-theme.css");
    // panini styles
    wp_enqueue_style("panini-structure", plugin_dir_url(__FILE__) . "/css/structural.css");
    wp_enqueue_style("panini-compiled-styles", plugin_dir_url(__FILE__) . "/frontend/css/styles.css");
}
add_action("wp_enqueue_scripts", "paniniFrontEndStyles");

function paniniFrontEndScripts() {
    // lib
    wp_enqueue_script("slick-slider", plugin_dir_url(__FILE__) . "/js/lib/slick/slick.min.js", array("jquery"));
    // panini scripts
    wp_enqueue_script("panini-front-end", plugin_dir_url(__FILE__) . "/js/front-end.js", array("jquery", "slick-slider"));
    wp_enqueue_script("panini-compiled-scripts", plugin_dir_url(__FILE__) . "/frontend/js/scripts.js", array("jquery", "slick-slider"), false, false);
}
add_action("wp_enqueue_scripts", "paniniFrontEndScripts");

/*
* Building wysiwyg editor
*/

// add button to toolbar in posts
function addPaniniButton() {
    echo '<span class="button" id="visual-page-builder-button"><i class="fa fa-newspaper-o"></i> Visual Page Builder</span>';
}
add_action("media_buttons", "addPaniniButton", 15);

function addPaniniMarkup() {
    include(plugin_dir_path(__FILE__) . "templates/admin-markup.php");
}
add_action("admin_footer", "addPaniniMarkup");


/*
* Utilities
*/

function paniniLoadAvailableBlocks($block = null) {
    // List of information to get from the file
    $headers = array(
        "name"              => "Block Name",
        "namespace"         => "Block Namespace",
        "identifier"        => "Block Identifier",
        "dependencies"      => "Block Dependencies",
        "classes"           => "Block Classes",
        "description"       => "Block Description"
    );
    // Creat an empty array to push block info to
    $availableBlocks = array();

    // look for individual block, or all blocks
    if ($block == null) {
        // look for all available blocks
        // get blocks in plugin directory first
        $pluginBlocks = glob(plugin_dir_path( __FILE__ ) . "blocks/**/*/block.php");
        // next get ones in the theme file
        $themeBlocks = glob(get_template_directory() . "/blocks/**/*/block.php");
        // last, get admin blocks
        $args = array(
            "post_type"         => "block",
            "posts_per_page"    => -1,
        );
        // we'll take care of this later
        $postBlocks = array();
        // merge all of them together
        $blocks = array_merge($pluginBlocks, $themeBlocks);
    } else {
        $blockUri = "";
        if (file_exists(plugin_dir_path( __FILE__ ) . "blocks/" . $block . "/block.php")) {
            $blockUri = plugin_dir_path( __FILE__ ) . "blocks/" . $block . "/block.php";
        }
        if (file_exists(get_template_directory() . "/blocks/" . $block . "/block.php")) {
            $blockUri = get_template_directory() . "/blocks/" . $block . "/block.php";
        }
        $blocks = array($blockUri);
    }

    // Loop through all blocks
    foreach ($blocks as $blockFile) {
        if (substr($blockFile, -9) == "block.php") {
            // this isn't the most foolproof way to do this, but it will work for now
            // get all specified file headers
            $blockData = get_file_data($blockFile, $headers);
            $availableBlocks[$blockData["namespace"] . "/" . $blockData["identifier"]] = $blockData;
            // get contents to pull all {{}} encased elements
            $contents = file_get_contents($blockFile);
        } else {
            // we'll assume that this is a block in a post type for now
        }

        // check for icon
        $iconPath = str_replace("block.php", "icon.png", $blockFile);
        if (file_exists($iconPath)) {
            $availableBlocks[$blockData["namespace"] . "/" . $blockData["identifier"]]["icon"] = $iconPath;
        }
        // check for a preview image
        $jpgPreview = str_replace("block.php", "preview.jpg", $blockFile);
        $pngPreview = str_replace("block.php", "preview.png", $blockFile);
        error_log($jpgPreview . " " . $pngPreview);
        if (file_exists($jpgPreview)) {
            $index = strpos($jpgPreview, "/wp-content");
            $previewUrl = substr($jpgPreview, $index);
        } else if(file_exists($pngPreview)) {
            $index = strpos($pngPreview, "/wp-content");
            $previewUrl = substr($pngPreview, $index);
        } else {
            $previewUrl = "/wp-content/plugins/paninipress/images/preview-default.jpg";
        }
        $availableBlocks[$blockData["namespace"] . "/" . $blockData["identifier"]]["preview"] = $previewUrl;
        
        preg_match_all('/({{)(.*)(}})/', $contents, $matches);
        // $matches[2] contains an array with each {{}} encased elements
        $atts = array();
        foreach ($matches[2] as $match) {
            $attributeInfo = explode(";", $match);
            $thisAtt = array();
            foreach ($attributeInfo as $info) {
                $keyValue = explode(":", trim($info));
                $thisAtt[$keyValue[0]] = $keyValue[1];
            }
            $atts[] = $thisAtt;
        }
        // put in to available blocks;
        $availableBlocks[$blockData["namespace"] . "/" . $blockData["identifier"]]["attributes"] = $atts;
    }

    // return that populated array,
    // keys in the array are namespace/identifier
    return $availableBlocks;
}

function paniniLoadAvailableBlocksHtml() {

    $blocks = paniniLoadAvailableBlocks();
    $html = "";
    $currentNamespace = "";
    foreach ($blocks as $block) {
        // heading
        if ($block["namespace"] != $currentNamespace) {
            $html .= '<span class="heading">' . $block["namespace"] . '</span>';
            $currentNamespace = $block["namespace"];
        }
        // icon
        $icon = "";
        if (array_key_exists("icon", $block)) {
            $iconUri = $block["icon"];
            $index = strpos($iconUri, "/wp-content/");
            $iconUrl = substr($iconUri, $index);
            $icon = '<img src="' . $iconUrl . '" class="block-icon" />';
        }
        // button text
        $html .= '<span class="button block-preview-button" data-identifier="' . $block["namespace"] . "/" . $block["identifier"] . '" data-preview="' . $block["preview"] . '" data-description="' . $block["description"] . '">' . $icon . $block["name"] . '</span>';
    }
    echo $html;
    wp_die();
}
add_action("wp_ajax_loadblockshtml", "paniniLoadAvailableBlocksHtml");

function loadBlockInputHtml() {
    $block = $_POST['block'];
    $blockData = array_shift(paniniLoadAvailableBlocks($block));
    // build html
    $html = '<div class="actionable">';
    // add template as hidden attribute
    $html .= '<input type="hidden" data-type="text" name="template" class="attribute" value="' . $_POST['block'] . '" data-label="Block" />';
    foreach ($blockData["attributes"] as $attribute) {
        // label
        $label = $attribute["name"];
        if (array_key_exists("label", $attribute)) {
            $label = $attribute["label"];
        }
        // sorting
        $sorting = 100;
        if (array_key_exists("position", $attribute)) {
            $sorting = $attribute["position"];
        }
        $html .= '<div class="input-wrapper" data-position="' . $sorting . '">';
        $html .= '<span class="label">' . $label . '</span>';
        switch ($attribute["type"]) {
            case "content":
                $html .= '<textarea class="block-content" data-type="content" name="' . $attribute["name"] . '" data-label="' . $label . '" /></textarea>';
                break;
            case "image":
                $html .= '<div class="image-input-wrapper">';
                $html .= '<input type="hidden" data-type="image" class="image-input attribute" name="' . $attribute["name"] . '" data-label="' . $label . '" />';
                $html .= '<div class="image-preview-wrapper fa fa-cloud-upload file-upload-trigger insert-media add_media"></div>';
                $html .= '<div class="image-input-actions file-upload-trigger insert-media add_media">';
                $html .= '<i class="fa fa-cloud-upload"></i> Change Image';
                $html .= '</div>';
                $html .= '</div>';
                break;
            case "spacing":
                $html .= '<div class="spacing-wrapper">
                    <input type="hidden" class="attribute" data-key="spacing" name="' . $attribute["name"] . '" data-type="spacing" data-label="' . $label . '" />
                    <div class="margin">
                        <div class="margin-top spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                        <div class="margin-right spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                        <div class="margin-bottom spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                        <div class="margin-left spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                    </div>
                    <div class="padding">
                        <div class="padding-top spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                        <div class="padding-right spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                        <div class="padding-bottom spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                        <div class="padding-left spacing-value" data-value="0">
                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                            <span class="value">0px</span>
                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                        </div>
                    </div>
                </div>';
                break;
            default:
                $html .= '<input type="text" data-type="text" class="attribute" name="' . $attribute["name"] . '" data-label="' . $label . '" />';
                break;
        }
        $html .= '</div>';
    }
    // update button
    $html .= '<span class="button button-primary insert-block">Add Block</span>';
    $html .= '</div>';
    $html .= '<div class="description">';
    $html .= '<span class="heading">' . $blockData["name"] . '</span>';
    $html .= '<img src="' . $blockData["preview"] . '" />';
    $html .= '<p>' . $blockData["description"] . '</p>';
    $html .= '</div>';
    // return html
    echo $html;
    wp_die();
}
add_action("wp_ajax_loadblockinputhtml", "loadBlockInputHtml");

/*
* Front end element output
*/

// rows
function paniniRenderRow($atts, $content = null) {
    // extract attributes with empty defaults
    $attributes = shortcode_atts(
        array(
            "styles"    => "",
            "classes"   => "",
            "spacing"   => "",
            "row-width" => "",
            "layout"    => "",
        ),
        $atts
    );
    // create class chain of classes, spacing and row width
    $classString = $attributes["layout"] . " " . $attributes["classes"] . " " . $attributes["spacing"] . " " . $attributes["row-width"];
    $html = '<div class="panini-row ' . trim(preg_replace('/\s\s+/', ' ', $classString)) . '" style="' . $atts["styles"] . '">';
    $html .= '<div class="row-content">';
    $html .= do_shortcode($content);
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}
add_shortcode('panini-row', 'paniniRenderRow');

// columns
function paniniRenderColumn($atts, $content = null) {
    // extract attributes with empty defaults
    $attributes = shortcode_atts(
        array(
            "styles"            => "",
            "classes"           => "",
            "spacing"           => "",
            "alignment"         => "",
            "column-type"       => false,
            "slides-to-show"    => "",
            "slider-timing"     => "",
            "show-dots"         => "",
            "show-arrows"       => ""
        ),
        $atts
    );
    // create class chain of classes, spacing and row width
    $classString = $attributes["classes"] . " " . $attributes["spacing"] . " " . $attributes["alignment"];
    $sliderString = "";
    $sliderClass = "";
    if ($attributes["column-type"] == "slider-column") {
        $sliderClass = " slider-column";
        $sliderString = ' data-slides-to-show="' . $attributes["slides-to-show"] . '"';
        $sliderString .= ' data-show-dots="' . $attributes["show-dots"] . '"';
        $sliderString .= ' data-show-arrows="' . $attributes["show-arrows"] . '"';
        $sliderString .= ' data-timing="' . $attributes["slider-timing"] . '"';
    }
    $html = '<div class="panini-column ' . trim(preg_replace('/\s\s+/', ' ', $classString)) . '">';
    $html .= '<div class="column-content' . $sliderClass . '"' . $sliderString . '>';
    $html .= do_shortcode($content);
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}
add_shortcode('panini-column', 'paniniRenderColumn');

// blocks
function splitValues($text) {
    $returnArray = array(
        "full-tag-value"    => $text
    );
    $text = substr($text, 2, strlen($text) - 4);
    $keyValuePairs = explode(";", $text);
    foreach ($keyValuePairs as $pair) {
        $keyValueArray = explode(":", trim($pair));
        if (count($keyValueArray) == 2) {
            $returnArray[$keyValueArray[0]] = $keyValueArray[1];
        } else if (count($keyValueArray) > 2) {
            // this could be for something like a URL that has : in the default value
            $key = array_shift($keyValueArray);
            $value = implode(":", $keyValueArray);
            $returnArray[$key] = $value;
        }
    }
    return $returnArray;
}

function insertValues($text, $atts, $content = null) {
    $regex = '/{{.+(}})/';
    preg_match_all($regex, $text, $matches);
    if ($content != null) {
        $contentArray = explode("[/content]", $content);
        foreach ($contentArray as $contentBlock) {
            if (trim($contentBlock) != "") {
                $contentBlock = trim($contentBlock);
                $index = strpos($contentBlock, "]");
                $contentText = trim(substr($contentBlock, $index + 1));
                $attributes = shortcode_parse_atts(substr($contentBlock, 0, $index));
                $name = $clear = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($attributes["name"]))))));
                $atts[$name] = $contentText;
            }
        }
    }
    foreach ($matches[0] as $match) {
        $values = splitValues($match);
        // set default or empty value
        $replaceValue = "";
        if (array_key_exists("default", $values)) {
            $replaceValue = $values["default"];
        }
        // check if they've specified anything
        if (array_key_exists($values["name"], $atts)) {
            $replaceValue = $atts[$values["name"]];
        }
        $text = str_replace($values["full-tag-value"], $replaceValue, $text);
    }
    return $text;
}

function paniniRenderBlock($atts, $content = null) {
    // get template
    if (array_key_exists("template", $atts)) {
        $template = $atts["template"];
    } else {
        return false;
    }
    // check for where this template is
    $fileString = "blocks/" . $template . "/block.php";
    $templatePath = false;
    if (file_exists(plugin_dir_path( __FILE__ ) . $fileString)) {
        $templatePath = plugin_dir_path( __FILE__ ) . $fileString;
    }
    if (file_exists(get_template_directory() . $fileString)) {
        $templatePath = get_template_directory() . $fileString;
    }
    if (!$templatePath) {
        return false;
    }

    // start output bufer
    ob_start();
    include($templatePath);
    $output = ob_get_contents();
    $output = insertValues($output, $atts, $content);
    ob_clean();

    // return processed output
    return $output;
};
add_shortcode("paniniblock", "paniniRenderBlock");


/*
* Admin Functions
*/

function paniniGenerateCss() {
    // get all available css from plugin folder and theme folder
    $pluginCss = glob(plugin_dir_path( __FILE__ ) . "blocks/**/*/css/styles.css");
    $themeCss = glob(get_template_directory() . "/blocks/**/*/css/styles.css");
    $allCss = array_merge($pluginCss, $themeCss);

    // loop through all css files
    if (gettype($allCss) == "array") {
        // destination file
        $mergedFilePath = plugin_dir_path( __FILE__ ) . "frontend/css/styles.css";
        $mergedContents = "";
        foreach ($allCss as $cssPath) {
            $mergedContents .= file_get_contents($cssPath) . "\n";
        }
        // older versions used append, we'll see how this one goes
        file_put_contents($mergedFilePath, $mergedContents);
    }
}

function paniniGenerateJavascript() {
    // get all available css from plugin folder and theme folder
    $pluginJs = glob(plugin_dir_path( __FILE__ ) . "blocks/**/*/js/scripts.js");
    $themeJs = glob(get_template_directory() . "/blocks/**/*/js/scripts.js");
    $allJs = array_merge($pluginJs, $themeJs);

    // loop through all css files
    if (gettype($allJs) == "array") {
        // destination file
        $mergedFilePath = plugin_dir_path( __FILE__ ) . "frontend/js/scripts.js";
        $mergedContents = "";
        foreach ($allJs as $jsPath) {
            $mergedContents .= file_get_contents($jsPath) . "\n";
        }
        // older versions used append, we'll see how this one goes
        file_put_contents($mergedFilePath, $mergedContents);
    }
}


// only do this on dev mode enabled
if (get_option("panini_dev_mode") == "on") {
    add_action("init", "paniniGenerateCss");
    add_action("init", "paniniGenerateJavascript");
}

// set ajax action for rebuilding css
add_action("wp_ajax_rebuildcss", "paniniGenerateJavascript");
add_action("wp_ajax_rebuildcss", "paniniGenerateCss");


/*
* Admin options
*/

function registerPaniniSettings() {
    register_setting('panini_options', 'panini_dev_mode');
    register_setting('panini_options', 'panini_block_destination');
    register_setting('panini_options', 'panini_colors');
}
add_action( 'admin_init', 'registerPaniniSettings' );

function paniniSettingsPage() {
    add_menu_page( "PaniniPress", "PaniniPress", "manage_options", 'paninipress', 'paniniSettingsPageCallback');
}

function paniniSettingsPageCallback() {
    ?>
    <div class="wrap">
        <form method="post" action="options.php">
            <?php settings_fields('panini_options'); ?>
            <?php do_settings_sections('panini_options'); ?>
            <h2>Utility Settings and Actions</h2>
            <table class="form-table">
                <tr valign="top">
                    <th>Developer Mode</th>
                    <td>
                        <div class="toggle-input">
                            <?php $devMode = get_option("panini_dev_mode"); ?>
                            <input type="radio" id="panini_dev_mode_off" name="panini_dev_mode" value="off"<?php if ($devMode == "off"): echo ' checked="checked"'; endif; ?> />
                            <label for="panini_dev_mode_off">Off</label>
                            <input type="radio" id="panini_dev_mode_on" name="panini_dev_mode" value="on"<?php if ($devMode == "on"): echo ' checked="checked"'; endif; ?> />
                            <label for="panini_dev_mode_on" data-vaue="on">On</label>
                        </div>
                        <p>Enabling developer mode will rebuild javascript, css, and incriment the cache number every time you reload a page. It is recommended that you enable this only on local environments and not on any production sites.</p>
                    </td>
                </tr>
                <tr valign="top">
                    <th>Rebuild Javascript</th>
                    <td>
                        <span class="button" id="rebuild-javascript">Rebuild JS</span>
                        <p>Rebuilding Javascript will take all scripts.js files within the block folders, combine them in to one and minify them. It will also rebuild the dependencies that each of theses script files needs. This is the script file that is enqueued on the page.</p>
                </tr>
                <tr valign="top">
                    <th>Rebuild CSS</th>
                    <td>
                        <span class="button" id="rebuild-css">Rebuild CSS</span>
                        <p>Rebuilding CSS will take all styles.css files within the block folders, combine them in to one and minify them. This is the styles file that is enqueued on the page.</p>
                </tr>
                <tr valign="top">
                    <th>Rebuild Icons Sprite/Styles</th>
                    <td>
                        <span class="button" id="rebuild-icons">Rebuild Icons</span>
                        <p>Rebuilding icons will take all of the current icon images and compile them in to a sprite, as well as rebuild all of the styles.</p>
                </tr>
            </table>
            <h2>Plugin Settings</h2>
            <table class="form-table">
                <tr valign="top">
                    <th>New Block Destination</th>
                    <td>
                        <div class="toggle-input">
                            <?php $destination = get_option("panini_block_destination"); ?>
                            <input type="radio" id="panini_block_dest_theme" name="panini_block_destination" value="theme"<?php if ($destination == "theme"): echo ' checked="checked"'; endif; ?> />
                            <label for="panini_block_dest_theme">Theme</label>
                            <input type="radio" id="panini_block_dest_plugin" name="panini_block_destination" value="plugin"<?php if ($destination == "plugin"): echo ' checked="checked"'; endif; ?> />
                            <label for="panini_block_dest_plugin" data-vaue="on">Plugin</label>
                        </div>
                        <p>Selecting <strong>theme</strong> will put all downloaded blocks in to your currently selected theme folder under <strong>/blocks</strong>. Selecting <strong>plugin</strong> will do the same, but within the PaniniPress plugin folder. There is no wrong selection, its a matter of preference.</p>
                </tr>
                <tr valign="top">
                    <th>Site Colors</th>
                    <td>
                        <p>Use this area to define colors that can be used throughout the site.</p>
                        <div id="option-area-colors">
                            <input type="hidden" name="panini_colors" value="<?php echo get_option("panini_colors"); ?>" />
                            <div class="color-list">
                                <?php
                                if (get_option("panini_colors")) {
                                    $colors = explode(",", get_option("panini_colors"));
                                    foreach ($colors as $color) {
                                        echo '<div class="color-swatch" style="background:#' . $color . ';">';
                                        echo '<label>#' . $color . '</label>';
                                        echo '</div>';
                                    }
                                }
                                ?>
                            </div>
                            <span class="button add-color"><i class="fa fa-plus"></i> Add Color</span>
                        </div>
                    </td>
                </tr>
            </table>
            <?php submit_button("Save Settings"); ?>
        </form>
    </div>
    <?php
}

add_action( 'admin_menu', 'paniniSettingsPage' );





/*
* App stuff
*/

include("app/icons.php");
// include("app/slides.php");