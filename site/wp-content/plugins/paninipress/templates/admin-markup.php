<div id="panini-modal">
    <div>

        <div id="close-modal">
            <i class="fa fa-close"></i>
        </div>

        <div class="content" data-content="blocks">
            <div class="tabs">
                <div class="tabs-nav">
                    <span class="tab"><i class="fa fa-th"></i> Available Blocks</span>
                    <span class="tab disabled" data-target="block-details"><i class="fa fa-stop"></i> Selected Block</span>
                </div>
                <div class="tabs-content">
                    <div id="blocks-list" class="dynamic-content tab-target">
                        <div class="actionable"></div>
                        <div class="description"></div>
                    </div>
                    <div id="block-details" class="tab-target">
                    </div>
                </div>
            </div>
        </div>


        <div class="content" data-content="row-details">
            <div id="row-details">
                <div class="tabs">
                    <div class="tabs-nav">
                        <span class="tab"><i class="fa fa-list"></i> Row Details</span>
                        <span class="tab"><i class="fa fa-bullseye"></i> Row Spacing</span>
                    </div>
                    <div class="tabs-content">
                        <!-- tab 1 -->
                        <div id="row-details" class="tab-target">
                            <div class="actionable">
                                <span class="heading">Content Width <i class="fa fa-question-circle more-info toggle-tab" data-target="row-width"></i></span>
                                <div class="details-wrapper">
                                    <div class="radio-wrapper">
                                        <input type="hidden" data-key="row-width" data-type="radio" class="attribute" />
                                        <span class="radio" data-value="full-width">Full Width</span>
                                        <span class="radio" data-value="page-width">Page Width</span>
                                        <span class="radio" data-value="large">Large</span>
                                        <span class="radio" data-value="medium">Medium</span>
                                        <span class="radio" data-value="small">Small</span>
                                        <span class="radio" data-value="x-small">X-Small</span>
                                    </div>
                                </div>
                                <div class="details-wrapper" data-detail="classes">
                                    <span class="heading">Additional Classes <i class="fa fa-question-circle more-info toggle-tab" data-target="additional-classes"></i></span>
                                    <input type="text" data-key="classes" class="attribute" />
                                </div>
                                <div class="details-wrapper" data-detail="styles">
                                    <span class="heading">Additional Styles <i class="fa fa-question-circle more-info toggle-tab" data-target="additional-styles"></i></span>
                                    <input type="text" data-key="styles" class="attribute" />
                                </div>
                                <span class="button button-primary update-row"><i class="fa fa-floppy-o"></i> Update Row</span>
                            </div>
                            <div class="description">
                                <div class="toggle-target" data-target="row-width">
                                    <span class="heading">Content Width</span>
                                    <p>This will determine how wide the content placed inside the row can be. Options smaller than <strong>page width</strong> will mostly apply only to single column rows. Options smaller than <strong>full width</strong> will be centered on the page unless otherwise specified in column or block styles.</p>
                                    <p><strong>Full Width</strong> - The content will stretch to the full width of the page container. If the page template is set to any full width template, this will be the full width of the browser window.</p>
                                    <p><strong>Page Width</strong> - The content will stretch to the full width of the page (no more than 1200px)</p>
                                    <p><strong>Large</strong> - The content will be no more than 980px wide.</p>
                                    <p><strong>Medium</strong> - The content will be no mroe than 780px wide.</p>
                                    <p><strong>Small</strong> - The content will be no more than 640px wide.</p>
                                    <p><strong>X-Small</strong> - The content will be no more than 480px wide.</p>
                                </div>
                                <div class="toggle-target" data-target="additional-classes">
                                    <span class="heading">Additional Classes</span>
                                    <p>Additional classes allow for additional styles to be applied to this column and any content within it. These are specified within existing style sheets. Unless you know specifically what classes to apply, leave this empty.</p>
                                </div>
                                <div class="toggle-target" data-target="additional-styles">
                                    <span class="heading">Additional Styles</span>
                                    <p>Additional styles let you apply inline styles to this particular column. These changes will only effect the column element itself and not necessarily any content within that column. <strong>Use this sparingly,</strong> as too much inline styles within a site can hurt SEO.</p>
                                </div>
                            </div>
                        </div>
                        <!-- tab 2 --><!-- tab 2 -->
                        <div id="row-spacing" class="tab-target spacing">
                            <div class="actionable">
                                <div class="spacing-wrapper">
                                    <input type="hidden" class="attribute" data-key="spacing" data-type="spacing" />
                                    <div class="margin">
                                        <div class="margin-top spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                        <div class="margin-right spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                        <div class="margin-bottom spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                        <div class="margin-left spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                    </div>
                                    <div class="padding">
                                        <div class="padding-top spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                        <div class="padding-right spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                        <div class="padding-bottom spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                        <div class="padding-left spacing-value" data-value="0">
                                            <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                            <span class="value">0px</span>
                                            <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="description">
                                <span class="heading">Row Spacing</span>
                                <p>Specify the spacing to use on this element. Margin will change the spacing between this element and other elements, while padding will change the space between the edges of this element and it's content.</p>
                                <p>tldr; <strong>Margin</strong> is <strong>outside</strong> of the element, while <strong>padding</strong> is <strong>inside</strong> of the element.</p>
                                <span class="button button-primary update-row"><i class="fa fa-floppy-o"></i> Update Row</span>
                            </div>
                        </div>
                        <!-- end of tabs -->
                    </div>
                </div>
            </div>
        </div>


        <div class="content" data-content="column-details">
            <div class="tabs">
                <div class="tabs-nav">
                    <span class="tab"><i class="fa fa-list"></i> Column Details</span>
                    <span class="tab"><i class="fa fa-bullseye"></i> Column Spacing</span>
                </div>
                <div class="tabs-content">
                    <!-- tab 1 -->
                    <div id="column-details" class="tab-target">
                        <div class="actionable">
                            <div class="details-wrapper" data-detail="alignment">
                                <span class="heading">Text Alignment <i class="fa fa-question-circle more-info toggle-tab" data-target="text-alignment"></i></span>
                                <div class="radio-wrapper">
                                    <input type="hidden" data-type="radio" data-key="alignment" class="attribute" />
                                    <span class="radio" data-value="align-left">Left</span>
                                    <span class="radio" data-value="align-center">Center</span>
                                    <span class="radio" data-value="align-right">Right</span>
                                </div>
                            </div>
                            <div class="details-wrapper" data-detail="type">
                                <input type="hidden" data-type="button" data-key="column-type" class="attribute" />
                                <span class="heading">Column Type <i class="fa fa-question-circle more-info toggle-tab" data-target="column-type"></i></span>
                                <span class="button column-type" data-target="column-default" data-value=""><i class="fa fa-ban"></i> Default</span>
                                <span class="button column-type" data-target="column-slider" data-value="slider-column"><i class="fa fa-sliders"></i> Slider</span>
                                <div class="type-details-wrapper">
                                    <div class="type-details" data-details="column-default">
                                        No additional information required
                                    </div>
                                    <div class="type-details" data-details="column-slider">
                                        <div class="counter" data-incriment="1">
                                            <label>Slides to Show</label>
                                            <i class="fa fa-caret-up arrow-up" data-action="add"></i>
                                            <i class="fa fa-caret-down arrow-down" data-action="minus"></i>
                                            <span class="value">0</span>
                                            <input type="hidden" data-key="slides-to-show" data-type="counter" class="attribute" />
                                        </div>
                                        <div class="counter" data-incriment="500">
                                            <label>Timing (miliseconds)</label>
                                            <i class="fa fa-caret-up arrow-up" data-action="add"></i>
                                            <i class="fa fa-caret-down arrow-down" data-action="minus"></i>
                                            <span class="value">0</span>
                                            <input type="hidden" data-key="slider-timing" data-type="counter" class="attribute" />
                                        </div>
                                        <div class="wrapper">
                                            <div class="radio-wrapper">
                                                <p class="label">Show Arrows</p>
                                                <input type="hidden" data-key="show-arrows" data-type="radio" class="attribute" />
                                                <span class="radio" data-value="true">Yes</span>
                                                <span class="radio" data-value="false">No</span>
                                            </div>
                                            <div class="radio-wrapper">
                                                <p class="label">Show Navigation Dots</p>
                                                <input type="hidden" data-key="show-dots" data-type="radio" class="attribute" />
                                                <span class="radio" data-value="true">Yes</span>
                                                <span class="radio" data-value="false">No</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="details-wrapper" data-detail="classes">
                                <span class="heading">Additional Classes <i class="fa fa-question-circle more-info toggle-tab" data-target="additional-classes"></i></span>
                                <input type="text" data-type="text" data-key="classes" class="attribute" />
                            </div>
                            <div class="details-wrapper" data-detail="styles">
                                <span class="heading">Additional Styles <i class="fa fa-question-circle more-info toggle-tab" data-target="additional-styles"></i></span>
                                <input type="text" data-type="text" data-key="styles" class="attribute" />
                            </div>
                            <span class="button button-primary update-column"><i class="fa fa-floppy-o"></i> Update Column</span>
                        </div>
                        <div class="description">
                            <div class="toggle-target" data-target="text-alignment">
                                <span class="heading">Text Alignment</span>
                                <p>This will set the text alignment on the column level. All text in this column will follow this rule unless specified by an invividual block within the column.</p>
                            </div>
                            <div class="toggle-target" data-target="column-type">
                                <span class="heading">Column Type</span>
                                <p>Column allows you to select different functions that this area of content may have. <br /> Available options are:</p>
                                <p><strong>Default -</strong> This is just a normal column with no specific function <br />
                                   <strong>Slider -</strong> This functions as a basic slider with some options. Any individual block that is put in to this column will become a slide</p>
                            </div>
                            <div class="toggle-target" data-target="column-default">
                                <span class="heading">Default Column Type</span>
                                <p>This is a normal structural column with no specific function.</p>
                            </div>
                            <div class="toggle-target" data-target="additional-classes">
                                <span class="heading">Additional Classes</span>
                                <p>Additional classes allow for additional styles to be applied to this column and any content within it. These are specified within existing style sheets. Unless you know specifically what classes to apply, leave this empty.</p>
                            </div>
                            <div class="toggle-target" data-target="additional-styles">
                                <span class="heading">Additional Styles</span>
                                <p>Additional styles let you apply inline styles to this particular column. These changes will only effect the column element itself and not necessarily any content within that column. <strong>Use this sparingly,</strong> as too much inline styles within a site can hurt SEO.</p>
                            </div>
                        </div>
                    </div>
                    <!-- tab 2 -->
                    <div id="column-spacing" class="tab-target spacing">
                        <div class="actionable">
                            <div class="spacing-wrapper">
                                <input type="hidden" data-type="spacing" class="attribute" data-key="spacing" />
                                <div class="margin">
                                    <div class="margin-top spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                    <div class="margin-right spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                    <div class="margin-bottom spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                    <div class="margin-left spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                </div>
                                <div class="padding">
                                    <div class="padding-top spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                    <div class="padding-right spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                    <div class="padding-bottom spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                    <div class="padding-left spacing-value" data-value="0">
                                        <i class="fa fa-caret-up spacing-action" data-action="add"></i>
                                        <span class="value">0px</span>
                                        <i class="fa fa-caret-down spacing-action" data-action="subtract"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <span class="heading">Column Spacing</span>
                            <p>Specify the spacing to use on this element. Margin will change the spacing between this element and other elements, while padding will change the space between the edges of this element and it's content.</p>
                            <p>tldr; <strong>Margin</strong> is <strong>outside</strong> of the element, while <strong>padding</strong> is <strong>inside</strong> of the element.</p>
                            <span class="button button-primary update-column"><i class="fa fa-floppy-o"></i> Update Column</span>
                        </div>
                    </div>
                    <!-- end of tabs -->
                </div>
            </div>
        </div>


    </div>
</div>