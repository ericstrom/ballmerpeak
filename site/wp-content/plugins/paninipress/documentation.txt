File Structure

User/Developer generated content

/paninipress/
  /blocks
    /{{namespace}}
      /{{blockname}}
        /css
	  styles.css
	/js
	  scripts.js
	block.php
	functions.php
	preview.(jpg/png)
	icon.png


PaniniPress is structured to be simple to add content to your block list. Of the files listed above, the only required file is block.php. This is where you’ll write the markup for your block. All other files listed above are optional and will be automatically included if they exist. Any block.php file that follows this structure will show up in the list of available blocks within the admin area.

block.php - This is where your block markup will exist, along with definitions of basic block information.

functions.php - Any additional functionality that is required for your block should be placed in this file. This acts much like a theme’s functions.php file.

preview.(jpg/png) - This will show as the preview in the admin area for this block. Either .png or .jpg file formats are accepted.

icon.png - This should be a small icon that will show up next to the title of the block in the available block list

/css/styles.css - Any styles applicable to your block should be placed in this file.

/js/scripts.js - Any javascript functionality should be placed in this file.


Plugin Generated Files

/paninipress/
  /frontend
    /css
      styles.css
      styles.min.css
    /js
      scripts.js
      scripts.min.js

All styles.css and scripts.js files within your blocks will be combined and minimized. They live within the /frontend folder and these are the files that are actually included on the front end page. This means that you don’t need to enqueue each one of your script or style files for your block. The files just need to exist and will automatically be included.


Creating a block

Each block needs to exist within a namespace and have a unique identifier within that namespace. This namespace should be somewhat descriptive, and the identifier should roughly describe what the block is. For example, a list of recent posts might have a namespace of posts and an identifier of recent_posts. Both namespaces and Identifiers should only contain letters, numbers, and dashes or underscores.

block.php

The block.php file contains all of the markup as well as some descriptive information about the block.

File Headers

The first thing that needs to be present in the block.php file are the file headers. These are:
Block Name - Required. This is what will show up in the available block list to an admin user. Unlike namespace and identifier, this can contain spaces.
Block Namespace - Required. This must match the namespace that the block is in.
Block Identifier - Required. This must match the identifier that the block is in.
Block Dependencies - Optional. If any additional scripts are involved that have dependencies on any other scripts such as jQuery, the dependencies should be listed here, separated by commas, and match the name that the script is registered as within wordpress.
Block Description - Optional. This is what will show up in the preview area to an admin user.

Example:
<?php
/*
* Block Name: Fancy Slider
* Block Namespace: sliders
* Block Identifier: fancy_slider
* Block Dependencies: jquery,slick-slider
* Block Description: This is a fancier slider than what’s included with the plugin
*/
?>

Block content

After your file headers, you can put whatever markup and php code that you’d like. This is also where you’ll define what elements that an admin user can specify.

User input

All user input within a block is defined within two brackets {{..}} and can have a number of different options. These don’t need to be in any particular order, but should be formatted as {{key1:value1; key2:value2; key3:value3}}. When a block is displayed on the front end, the entire tag is replaced with whatever value the admin user has defined. If no input is defined, it will erase the tag.

Available options:

name - Required. This is the only required option and will match the attribute value that PaniniPress is pulling from the shortcode. This should only contain letters, numbers, dashes, and underscores.

label - This is the label that will show up for the input in the admin area and can be more descriptive than the name. Spaces are allowed in this value.

type - Not all values that a user input should be straight text. If this value isn’t specified, the input will default to a text input. Available types are:
	text - regular text input
	content - textarea input
	spacing - this will show the spacing grid
	image - this will show the preview of any selected image, and uses the default media modal for image selection.

position - This will specify the order in which to display the inputs to the admin user. If position is undefined, the inputs will be displayed in the order in which they’re defined within the block.php file.

Example:

{{name:background_image; type:image; label:Full Width Background Image; position:2}}

A full block file will look something like this:

<?php
/*
* Block Name: Image
* Block Namespace: basic
* Block Identifier: image
* Block Dependencies:
* Block Description: Simple image wrapped in a div element
*/
?>
<div class="image-wrapper {{name:additional-classes; type:text; position:4}}" 
     style="{{name:additional-styles; type:text; position:3}}">
    <img src="{{name:image; type:image; position:1}}" 
         alt="{{name:alt-text; type:text; position:2}}" />
</div>

