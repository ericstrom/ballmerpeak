/*
* Hero scrolling bits
*/

jQuery(document).ready(function($) {

    $(window).scroll(function() {
        if ($(window).scrollTop() > 30) {
            $(".hero").addClass("scroll");
        } else {
            $(".hero").removeClass("scroll");
        }
    });

});
