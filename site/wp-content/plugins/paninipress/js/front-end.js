/*
* Front end Panini scripts
*/

// sliders
jQuery(document).ready(function($){

    // there may be more than one slider on the page
    // because of this we're going to loop through each slider
    $(".slider-column").each(function() {
        $(this).slick({
            autoplay: true,
            slidesToShow: $(this).attr("data-slides-to-show"),
            autoplaySpeed: $(this).attr("data-timing"),
            arrows: ($(this).attr("data-show-arrows") == "true"),
            dots: ($(this).attr("data-show-dots") == "true")
        });
    });

});