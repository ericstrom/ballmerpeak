/*
* Panini Press admin scripts
 */
jQuery(document).ready(function($) {

    /*
    * Global(ish) variables
     */

    // editor object to make it easy
    var wysiwyg = $(".wp-editor-area#content");
    var paniniEditor;

    // content
    var blankRow = '<div class="panini-row">';
    blankRow += '<div class="row-options">';
    blankRow += '<div class="option" data-option="row-full" data-columns="1"><img src="/wp-content/plugins/paninipress/images/icons/row-full-width.jpg" /></div>';
    blankRow += '<div class="option" data-option="row-half" data-columns="2"><img src="/wp-content/plugins/paninipress/images/icons/row-half.jpg" /></div>';
    blankRow += '<div class="option" data-option="row-third" data-columns="3"><img src="/wp-content/plugins/paninipress/images/icons/row-third.jpg" /></div>';
    blankRow += '<div class="option" data-option="row-quarter" data-columns="4"><img src="/wp-content/plugins/paninipress/images/icons/row-quarter.jpg" /></div>';
    blankRow += '<div class="option" data-option="row-sidebar-left" data-columns="2"><img src="/wp-content/plugins/paninipress/images/icons/row-sidebar-left.jpg" /></div>';
    blankRow += '<div class="option" data-option="row-sidebar-right" data-columns="2"><img src="/wp-content/plugins/paninipress/images/icons/row-sidebar-right.jpg" /></div>';
    blankRow += '</div>';
    blankRow += '<div class="row-actions">';
    blankRow += '<div class="action" data-action="sort-up"><i class="fa fa-arrow-up"></i></div>'
    blankRow += '<div class="action" data-action="sort-down"><i class="fa fa-arrow-down"></i></div>'
    blankRow += '<div class="action" data-action="delete"><i class="fa fa-trash"></i></div>';
    blankRow += '<div class="action" data-action="settings"><i class="fa fa-cogs"></i></div>';
    blankRow += '</div>';
    blankRow += '<div class="row-data"></div>';
    blankRow += '<div class="row-content"></div>';
    blankRow += '</div>';

    var blankColumn = '<div class="panini-column">';
    blankColumn += '<div class="column-options">';
    blankColumn += '<div class="action" data-action="settings"><i class="fa fa-cogs"></i></div>';
    blankColumn += '<div class="action" data-action="sort"><i class="fa fa-arrows"></i></div>';
    blankColumn += '</div>';
    blankColumn += '<div class="column-content"></div>';
    blankColumn += '<div class="column-data"></div>';
    blankColumn += '<div class="add-block-button"><i class="button fa fa-plus"><span>Add Block</span></i></div>';
    blankColumn += '</div>';

    var blankBlock = '<div class="panini-block">';
    blankBlock += '<div class="block-data"></div>';
    blankBlock += '<div class="block-actions">';
    blankBlock += '<span class="action" data-action="sort"><i class="fa fa-arrows"></i></span>';
    blankBlock += '<span class="action" data-action="edit-block"><i class="fa fa-pencil"></i></span>';
    blankBlock += '<span class="action" data-action="delete-block"><i class="fa fa-trash"></i></span>';
    blankBlock += '</div>';
    blankBlock += '</div>';

    /*
    * Modal functions
    */

    function modalOpen(content) {
        $("#panini-modal").fadeIn();
        $("#panini-modal").find('[data-content="' + content + '"]').addClass("active");
        // click first tab if needed
        $("#panini-modal").find(".content.active").find(".tab").first().click();
        // run any content specific functions
        switch (content) {
            case "blocks":
                var blocksHtml = loadAvailableBlocksHtml();
                $("#blocks-list").html(blocksHtml);
                break;
            default:
                // no default one for right now
                // maybe in the future
                break;
        }
    }

    function modalClose() {
        $("#panini-modal").fadeOut(function() {
            $("#panini-modal").find(".dyanmic-content").html("");
            $("#panini-modal").find(".active").removeClass("active");
        });
    }

    function openContent(content) {
        $("#panini-modal").find(".active").removeClass("active");
        $("#panini-modal").find('[data-content="' + content + ']').addCLass("active");
    }

    $("#close-modal, .close-modal").click(function() {
        modalClose();
    });

    /*
    * User interactions
    */

    // visual page builder button
    $("#visual-page-builder-button").click(function() {
        // clear everything out
        $("#ed_toolbar").addClass("panini-hide");
        $(".mce-tinymce").addClass("panini-hide");
        $(wysiwyg).addClass("panini-hide");
        // add our container
        if ($("#panini-container").length == 0) {
            var containerHtml = '<div id="panini-container">';
            containerHtml += '<div class="content"></div>';
            containerHtml += '<div class="add-row"><i class="button fa fa-plus"></i></div>';
            containerHtml += '</div>';
            $("#wp-content-editor-container").append(containerHtml);
            paniniEditor = $("#panini-container");
        }
        buildVisualElements();
    });

    // add row button
    $(document).on("click", ".add-row", function() {
        $("#panini-container .content").append(blankRow);
    });

    // select row layout
    $(document).on("click", ".row-options .option", function() {
        if ($(this).hasClass("selected")) {
            return;
        }
        // apply correct styles
        $(this).siblings(".selected").removeClass("selected");
        $(this).addClass("selected");
        // put active row in to an object
        var activeRow = $(this).parents(".panini-row");
        // clear out content
        $(activeRow).find(".row-content").html("");
        // apply correct class
        $(activeRow).attr("data-layout", $(this).attr("data-option"));
        // build out columns
        var columnCount = parseInt($(this).attr("data-columns"));
        for (var i = 0; i < columnCount; i++) {
            $(activeRow).find(".row-content").append(blankColumn);
        }
        // build out shortcodes
        buildShortcodes();
    });

    // block preview button
    $(document).on("click", ".block-preview-button", function() {
        var html = '<img src="' + $(this).attr("data-preview") + '" class="preview-image" />';
        html += '<span class="heading">' + $(this).text() + '</span>';
        html += '<p>' + $(this).attr("data-description") + '</p>';
        html += '<span class="button button-primary select-block" data-block="' + $(this).attr("data-identifier") + '">Select This Block</span>';
        $("#blocks-list").find(".description").html(html);
    });

    // block select button
    $(document).on("click", ".select-block", function() {
        var block = $(this).attr("data-block");
        var targetTab = $(".tab[data-target='block-details']");
        $(targetTab).removeClass("disabled");
        $(targetTab).click();
        selectBlock(block);
    });

    // add block button within column
    $(document).on("click", ".add-block-button", function() {
        // set correct classes
        $(".panini-column.active").removeClass("active");
        $(this).parents(".panini-column").addClass("active");
        // clear out content
        $("#blocks-list .actionable").html("");
        $("#blocks-list .description").html("");
        // open modal
        modalOpen("blocks");
        loadAvailableBlocksHtml();
    });

    // delete block button
    $(document).on("click", '[data-action="delete-block"]', function() {
        var parentBlock = $(this).parents(".panini-block");
        if (confirm("Are you sure you want to delete this block?")) {
            $(parentBlock).remove();
            buildShortcodes();
        }
    });

    // edit block button
    $(document).on("click", '[data-action="edit-block"]', function() {
        // set active block
        $(".panini-block.active").removeClass("active");
        var blockObj = $(this).parents(".panini-block");
        $(blockObj).addClass("active");
        // open modal
        modalOpen("blocks");
        $('[data-target="block-details"]').removeClass("disabled");
        $('[data-target="block-details"]').click();
        // get block info
        var template = $(blockObj).find("input[name='template']").val();
        selectBlock(template, true);
    });

    // row settings
    $(document).on("click", ".row-actions [data-action='settings']", function() {
        // set correct active row
        $(".panini-row.active").removeClass("active");
        $(this).parents(".panini-row").addClass("active");
        // open the modal
        modalOpen("row-details");
        // clear out all inputs
        $("#row-details, #row-spacing").find("input").val("");
        $("#row-details").find(".radio.checked").removeClass("checked");
        $("#row-spacing").find(".value").html("0px");
        $("#row-spacing").find(".spacing-value").attr("data-value", "0");
        // get all of the info from the current row
        $(".panini-row.active").find(".row-data").find(".attribute").each(function() {
            var targetInput = $("#row-details, #row-spacing").find("[data-key='" + $(this).attr("name") + "']");
            // do what we need to for various input types
            switch ($(targetInput).attr("data-type")) {
                case "spacing":
                    var spacingArray = spacingToArray($(this).val());
                    $.each(spacingArray, function(spacingKey, spacingValue) {
                        var targetElement = $(targetInput).parents(".spacing-wrapper").find(".spacing-value." + spacingKey);
                        $(targetElement).attr("data-value", spacingValue);
                        $(targetElement).find(".value").html(spacingValue + "px");
                    });
                    break;
                case "radio":
                    $(targetInput).siblings(".radio").removeClass("checked");
                    $(targetInput).siblings(".radio[data-value='" + $(this).val() + "']").addClass("checked");
                    $(targetInput).val($(this).val());
                    break;
                default:
                    $(targetInput).val($(this).val());
                    break;
            }
        });
    });

    // column settings
    $(document).on("click", ".column-options [data-action='settings']", function() {
        // set correct active column
        $(".panini-column.active").removeClass("active");
        $(this).parents(".panini-column").addClass("active");
        // open the modal
        modalOpen("column-details");

        // clear out all inputs
        $("#column-details, #column-spacing").find("input").val("");
        $("#column-details").find(".button.active").removeClass("active");
        $("#column-details").find(".radio.checked").removeClass("checked");
        $("#column-spacing").find(".value").html("0px");
        $("#column-spacing").find(".spacing-value").attr("data-value", "0");
        // get all of the info from the current row
        $(".panini-column.active").find(".column-data").find(".attribute").each(function() {
            var targetInput = $("#column-details, #column-spacing").find("[data-key='" + $(this).attr("name") + "']");
            // do what we need to for various input types
            switch ($(targetInput).attr("data-type")) {
                case "spacing":
                    var spacingArray = spacingToArray($(this).val());
                    $.each(spacingArray, function(spacingKey, spacingValue) {
                        var targetElement = $(targetInput).parents(".spacing-wrapper").find(".spacing-value." + spacingKey);
                        $(targetElement).attr("data-value", spacingValue);
                        $(targetElement).find(".value").html(spacingValue + "px");
                    });
                    $(targetInput).val($(this).val());
                    break;
                case "radio":
                    $(targetInput).siblings(".radio").removeClass("checked");
                    $(targetInput).siblings(".radio[data-value='" + $(this).val() + "']").addClass("checked");
                    $(targetInput).val($(this).val());
                    break;
                case "counter":
                    $(targetInput).val($(this).val());
                    $(targetInput).siblings(".value").html($(this).val());
                    break;
                case "button":
                    $(targetInput).siblings(".button[data-value='" + $(this).val() + "']").click();
                    break;
                default:
                    $(targetInput).val($(this).val());
                    break;
            }
        });
    });

    /*
    * Spacing
    */

    $(document).on("click", ".spacing-action", function() {
        var action = $(this).attr("data-action");
        var currentValue = parseInt($(this).parents(".spacing-value").attr("data-value"));
        if (action == "add" && currentValue < 120) {
            currentValue = currentValue + 5;
        } else if (action == "subtract" && currentValue != 0) {
            currentValue = currentValue - 5;
        }
        $(this).parents(".spacing-value").attr("data-value", currentValue);
        $(this).siblings("span.value").html(currentValue + "px");
        var spacingClasses = getMarginPadding($(this).parents(".spacing-wrapper"));
        $(this).parents(".spacing-wrapper").find("input[type='hidden']").val(spacingClasses);
    });

    /*
    * Utilities
    */

    $(".update-row").click(function() {
        updateRow();
        modalClose();
    });

    function updateRow() {
        // put active row in to object
        var activeRow = $(".panini-row.active");
        // clear out current row data
        $(activeRow).find(".row-data").html("");
        // get all attributes and values
        $("#row-details, #row-spacing").find(".attribute").each(function() {
            if ($(this).val() == "") {
                // if empty, we don't want to include this in the data
                // just skip to the next one
                return true;
            }
            var inputHtml = '<input type="hidden" class="attribute" name="' + $(this).attr("data-key") + '" value="' + $(this).val() + '" />';
            $(activeRow).find(".row-data").append(inputHtml);
        });
        // data is set for this element, build out the shortcodes
        buildShortcodes();
    }

    $(".update-column").click(function() {
        updateColumn();
        modalClose();
    });

    function updateColumn() {
        // put active row in to object
        var activeColumn = $(".panini-column.active");
        // clear out current row data
        $(activeColumn).find(".column-data").html("");
        // get all attributes and values
        $("#column-details, #column-spacing").find(".attribute").each(function() {
            if ($(this).val() == "") {
                // if empty, we don't want to include this in the data
                // just skip to the next one
                return true;
            }
            console.log($(this).attr("name"));
            var inputHtml = '<input type="hidden" class="attribute" name="' + $(this).attr("data-key") + '" value="' + $(this).val() + '" />';
            $(activeColumn).find(".column-data").append(inputHtml);
        });
        // data is set for this element, build out the shortcodes
        buildShortcodes();
    }

    function buildVisualElements() {
        // this takes all of the shortcodes in the editor and converts them in to visual elements
        // this will hold all current text as is
        var currentText = $(wysiwyg).val();

        // get rows
        var rowsRegex = /\[panini-row[\s\S]+?(?=\[\/panini-row\])/g;
        var rows = currentText.match(rowsRegex);

        // loop through rows
        $.each(rows, function(key, rowText) {
            // add new row
            $("#panini-container .content").append(blankRow);
            var activeRow = $(".panini-row").last();
            // loop through attributes
            var rowAtts = getOpeningShortcode(rowText);
            $.each(rowAtts, function(key, value) {
                switch(key) {
                    case "layout":
                        $(activeRow).find('.row-options .option[data-option="' + value + '"]').click();
                        break;
                    default:
                        var rowAttributeHtml = '<input type="hidden" class="attribute" name="' + key + '" value="' + value + '" />';
                        $(activeRow).find(".row-data").append(rowAttributeHtml);
                        break;
                }
            });
            // get columns within row
            var columnsRegex = /\[panini-column[\s\S]+?(?=\[\/panini-column\])/g;
            var columns = rowText.match(columnsRegex);
            var rowColumns = $(activeRow).find(".panini-column");
            $.each(columns, function(key, columnText) {
                var columnAtts = getOpeningShortcode(columnText);
                var activeColumn = $(rowColumns).eq(key);
                $.each(columnAtts, function(key, value) {
                    switch(key) {
                        case "layout":
                            $(activeColumn).find('.row-options .option[data-option="' + value + '"]').click();
                            break;
                        default:
                            var columnAttributeHtml = '<input type="hidden" class="attribute" name="' + key + '" value="' + value + '" />';
                            $(activeColumn).find(".column-data").append(columnAttributeHtml);
                            break;
                    }
                });
                var blockRegex = /\[paniniblock[\s\S]+?(?=\[\/paniniblock\])/g;
                var blocks = columnText.match(blockRegex);
                $.each(blocks, function(key, blockText) {
                    $(activeColumn).find(".column-content").append(blankBlock);
                    var activeBlock = $(".panini-block").last();
                    var blockAtts = getOpeningShortcode(blockText);
                    // get attribute data and delte that as a key
                    var attributeInfoArray = blockAtts["attribute-data"].split("|");
                    var attributeData = {};
                    $.each(attributeInfoArray, function(key, value) {
                        var splitAtts = value.split(";");
                        var infoArray = {};
                        $.each(splitAtts, function(key, value) {
                            var keyValue = value.split(":");
                            infoArray[keyValue[0]] = keyValue[1];
                        });
                        attributeData[infoArray["name"]] = infoArray;
                    });
                    delete blockAtts["attribute-data"];
                    // loop through each attribute
                    $.each(blockAtts, function(key, value) {
                        var inputHtml = '<div class="attribute-wrapper">';
                        switch (attributeData[key]["type"]) {
                            case "spacing":
                                var iconClass = "fa-clone";
                                var valueHtml = value;
                                break;
                            case "image":
                                var iconClass = "fa-picture-o";
                                var valueHtml = '<img src="' + value + '" class="block-image-preview" />';
                                break;
                            default:
                                var iconClass = "fa-info-circle";
                                var valueHtml = value;
                                break;
                        }
                        inputHtml += '<span class="attribute-label"><i class="fa ' + iconClass + '"></i> ' + attributeData[key]["label"] + '</span>';
                        inputHtml += '<span class="attribute-value">' + valueHtml + '</span>';
                        inputHtml += '<input type="hidden" data-label="' + attributeData[key]["label"] + '" data-type="' + attributeData[key]["type"] + '" name="' + attributeData[key]["name"] + '" class="block-attribute" value="' + value + '" />';
                        inputHtml += '</div>';
                        // put in to block
                        $(activeBlock).find(".block-data").append(inputHtml);
                    });
                    // get any content
                    var contentRegex = /\[content[\s\S]+?(?=\[\/content\])/g;
                    var content = blockText.match(contentRegex);
                    $.each(content, function(key, contentText) {
                        var contentAtts = getOpeningShortcode(contentText);
                        var index = contentText.indexOf("]");
                        var contentVal = contentText.substring(index + 1).trim();
                        var inputHtml = '<span class="attribute-label"><i class="fa fa-align-left"></i> ' + contentAtts["label"] + '</span>';
                        inputHtml += '<div class="block-content" data-name="' + contentAtts["name"] + '" data-label="' + contentAtts["label"] + '">';
                        inputHtml += contentVal;
                        inputHtml += '</div>';
                        $(activeBlock).find(".block-data").append(inputHtml);
                    });
                });
            });
        });

        // now that all of the visual elements are there, build the shortcodes again
        // this is done because some of the ways that the visual elements are built triggers new shortcode building
        // it also helps to make sure that the process back and forth makes the same text
        // this is mostly debugging stuff but oh well
        buildShortcodes();

    }

    function buildShortcodes() {
        // this takes all visual elements and converts them in to shortcodes
        // sorry, this is going to be a lot of nested loops
        var outputText = "";
        $(".panini-row").each(function() {
            // start off with the rows
            var rowText = '[panini-row layout="' + $(this).attr("data-layout") + '" ';
            var rowDataObj = $(this).find(".row-data .attribute");
            var rowDataArray = Array();
            $.each(rowDataObj, function() {
                rowDataArray.push($(this).attr("name") + '="' + $(this).val() + '"');
            });
            rowText += rowDataArray.join(" ") + "]\n";

            // we have our row shortcode,
            // start on the columns within the row
            $(this).find(".panini-column").each(function() {
                var columnText = '[panini-column ';
                var columnDataObj = $(this).find(".column-data .attribute");
                var columnDataArray = Array();
                $.each(columnDataObj, function() {
                    columnDataArray.push($(this).attr("name") + '="' + $(this).val() + '"');
                });
                columnText += columnDataArray.join(" ") + "]\n";

                // we have our column shortcode,
                // start on blocks within the column
                $(this).find(".panini-block").each(function() {
                    var blockText = '[paniniblock ';
                    var blockDataObj = $(this).find(".block-data .block-attribute");
                    var blockDataArray = Array();
                    // empty array for attribute info
                    var attributeInfo = Array();
                    // set up empty array for
                    $.each(blockDataObj, function() {
                        var attributeInfoString = "name:" + $(this).attr("name") + ";type:" + $(this).attr("data-type") + ";label:" + $(this).attr("data-label");
                        attributeInfo.push(attributeInfoString);
                        blockDataArray.push($(this).attr("name") + '="' + $(this).val() + '"');
                    });
                    blockDataArray.push('attribute-data="' + attributeInfo.join("|") + '"');
                    blockText += blockDataArray.join(" ") + "]";
                    // add any content that may exist
                    $(this).find(".block-content").each(function() {
                        blockText += '\n[content name="' + $(this).attr("data-name") + '" label="' + $(this).attr("data-label") + '"]\n';
                        blockText += $(this).html();
                        blockText += '\n[/content]\n';
                    });
                    // close block
                    blockText += '[/paniniblock]\n';
                    // add to column text
                    columnText += blockText;
                });

                //close column
                columnText += '[/panini-column]\n';
                // add to row text
                rowText += columnText;
            });

            // close row
            rowText += '[/panini-row]\n\n';
            // add to output
            outputText += rowText;
        });
        // output to wysiwyg
        $(wysiwyg).val(outputText);

        // sortable stuff
        $(".column-content").sortable({
            stop: function(event, ui) {
                buildShortcodes();
            }
        });
    }

    // gets JSON of available blocks
    function loadAvailableBlocks() {

    }

    // gets html output of those blocks;
    function loadAvailableBlocksHtml() {
        // clear the data
        $("#blocks-list").find(".actionable").html("");
        $("#blocks-list").find(".actionable").addClass("loading");
        // ajax call
        var senddata = {
            "action": "loadblockshtml"
        };
        $.post(panini_ajax_object.ajax_url, senddata, function(data) {
            // add rendered html
            $("#blocks-list").find(".actionable").removeClass("loading");
            $("#blocks-list").find(".actionable").html(data);
        });
    }

    function selectBlock(block, editblock = false) {
        // clear the data
        $("#block-details").html("");
        $("#block-details").addClass("loading");
        var senddata = {
            "action": "loadblockinputhtml",
            "block": block
        };
        $.post(panini_ajax_object.ajax_url, senddata, function(data) {
            $("#block-details").removeClass("loading");
            // add the input elements
            $("#block-details").html(data);
            // add the description
            $("#block-details .input-wrapper").sort(sortElements).prependTo("#block-details .actionable");
            
            // this next bit only fires if you're editing the block
            // gets all the values of the block and places them in to the inputs
            if (editblock) {
                // change the button to an edit block button, not an insert block button
                $("#block-details").find(".insert-block").addClass("edit-block");
                $("#block-details").find(".edit-block").html("Update Block");
                // do your thing
                var editingBlock = $(".panini-block.active");
                $(editingBlock).find(".block-attribute, .block-content").each(function() {
                    var value = $(this).val();
                    var name = $(this).attr("name");
                    var targetInput = $("#block-details").find("input[name='" + name + "']");
                    if ($(this).hasClass("block-content")) {
                        // figure out a less lame way to do this
                        // getting html if its block content instead of .val()
                        value = $(this).html();
                        name = $(this).attr("data-name");
                        targetInput = $("#block-details").find("textarea[name='" + name + "']");
                        // again, lets make this better later
                    }
                    var inputType = $(targetInput).attr("data-type");
                    switch (inputType) {
                        case "content":
                            $(targetInput).html(value);
                            break;
                        case "spacing":
                            var spacingArray = spacingToArray(value);
                            $.each(spacingArray, function(spacingKey, spacingValue) {
                                var targetElement = $(targetInput).parents(".spacing-wrapper").find(".spacing-value." + spacingKey);
                                $(targetElement).attr("data-value", spacingValue);
                                $(targetElement).find(".value").html(spacingValue + "px");
                            });
                            break;
                        case "image":
                            $(targetInput).siblings(".image-preview-wrapper").html('<img src="' + value + '" />');
                            $(targetInput).val(value);
                            break;
                        default:
                            $(targetInput).val(value);
                            break;
                    }
                });
            }
        });
    }

    $(document).on("click", ".insert-block", function() {
        // determine if we're editing the block or not
        var editing = false;
        if ($(this).hasClass("edit-block")) {
            editing = true;
        }
        // convert to visual
        insertBlock(editing);
        modalClose();
    });
    function insertBlock(editing = false) {
        // put blank block in to active column wrapper
        if (editing) {
            var activeBlock = $(".panini-block.active");
            // clear out current values
            $(activeBlock).find(".block-data").html("");
        } else {
            $(".panini-column.active .column-content").append(blankBlock);
            var activeBlock = $(".panini-column.active .panini-block").last();
        }
        $("#block-details").find(".attribute").each(function() {
            if ($(this).val() == "") {
                return true;
            }
            var inputHtml = '<div class="attribute-wrapper">';
            switch ($(this).attr("data-type")) {
                case "spacing":
                    var iconClass = "fa-clone";
                    var valueHtml = $(this).val();
                    break;
                case "image":
                    var iconClass = "fa-picture-o";
                    var valueHtml = '<img src="' + $(this).val() + '" class="block-image-preview" />';
                    break;
                default:
                    var iconClass = "fa-info-circle";
                    var valueHtml = $(this).val();
                    break;
            }
            inputHtml += '<span class="attribute-label"><i class="fa ' + iconClass + '"></i> ' + $(this).attr("data-label") + '</span>';
            inputHtml += '<span class="attribute-value">' + valueHtml + '</span>';
            inputHtml += '<input type="hidden" data-label="' + $(this).attr("data-label") + '" data-type="' + $(this).attr("data-type") + '" name="' + $(this).attr("name") + '" class="block-attribute" value="' + $(this).val() + '" />';
            inputHtml += '</div>';
            $(activeBlock).find(".block-data").append(inputHtml);
        });
        $("#block-details").find(".block-content").each(function() {
            var inputHtml = '<span class="attribute-label"><i class="fa fa-align-left"></i> ' + $(this).attr("data-label") + '</span>';
            inputHtml += '<div class="block-content" data-name="' + $(this).attr("name") + '" data-label="' + $(this).attr("data-label") + '">';
            inputHtml += $(this).val();
            inputHtml += '</div>';
            $(activeBlock).find(".block-data").append(inputHtml);
        });
        buildShortcodes();
    }

    /*
    * Tabs
    */

    $(".tab").click(function() {
        if ($(this).hasClass("disabled")) {
            return false;
        }
        var collection = $(this).parents(".tabs-nav").find(".tab");
        var index = $(collection).index(this);
        $(collection).removeClass("active");
        $(this).addClass("active");
        var content = $(this).parents(".tabs").find(".tab-target");
        $(content).removeClass("active");
        $(content).eq(index).addClass("active");
    });

    $(".toggle-tab").click(function() {
        var targetName = $(this).attr("data-target");
        var target = $(".toggle-target[data-target='" + targetName + "']");
        $(target).siblings(".active").removeClass("active");
        $(target).addClass("active");
    });

    // column type buttons
    $(".column-type").click(function() {
        // don't clear everything out if its already selected
        if ($(this).hasClass("active")) {
            return;
        }
        // check which one is being set and set that attribute
        var target = $(this).attr("data-target");
        var value = $(this).attr("data-value");
        $(this).siblings("[data-key='column-type']").val(value);
        // set visual stuff
        $(this).siblings(".active").removeClass("active");
        $(this).addClass("active");
        $(".type-details.active").removeClass("active");
        $(".type-details[data-details='" + target + "']").addClass("active");
        // clear out all input
        $(".type-details").find("input").val("");
        $(".type-details").find(".value").html("0");
        $(".type-details").find(".radio.checked").removeClass("checked");
    });

    /*
    * Fake form elements
    */

    // radio buttons
    $(".radio").click(function() {
        $(this).siblings(".radio").removeClass("checked");
        $(this).addClass("checked");
        var value = $(this).attr("data-value");
        $(this).siblings(".attribute").val(value);
    });

    // counters
    $(".counter i").click(function() {
        var incriment = parseInt($(this).parents(".counter").attr("data-incriment"));
        var currentValue = $(this).siblings(".attribute").val();
        if (typeof currentValue == "undefined" || currentValue == null || currentValue == "") {
            currentValue = 0;
        }
        if ($(this).attr("data-action") == "add") {
            var newValue = parseInt(currentValue) + incriment;
        } else if ($(this).attr("data-action") == "minus" && currentValue != 0) {
            var newValue = parseInt(currentValue) - incriment;
        }
        // put value in to inputs
        $(this).siblings(".attribute").val(newValue);
        $(this).siblings(".value").html(newValue);
    });

    // Files
    $(document).on("click", ".file-upload-trigger", function() {
        // clear out old value
        var textInput = $(this).siblings(".image-input");
        var preview = $(this).parents(".image-input-wrapper").find(".image-preview-wrapper");
        // preivew is handled like this so that the preview wrapper itself also acts as a trigger
        window.send_to_editor = function(html) {
            var regex = /<img.*?src="(.*?)"/;
            var src = regex.exec(html)[1];
            src = src.replace("http://", "");
            src = src.replace("https://", "");
            src = src.replace(window.location.hostname, "");
            $(textInput).val(src);
            $(preview).html('<img src="' + src + '" />');
        }
    });

    /*
    * Helpers
    */

    // sort elements by data value
    function sortElements(a, b) {
        return ($(b).attr("data-position")) < ($(a).attr("data-position")) ? 1 : -1;
    }

    function getOpeningShortcode(string) {
        var index = string.indexOf("]");
        var shortcode = string.substr(0, index + 1);
        var attributes = getAttributes(shortcode);
        return attributes;
    }

    function getAttributes(string) {
        var regex = /[a-zA-Z_-]+=(["'])(?:(?=(\\?))\2.)*?\1/g;
        var attributes = string.match(regex);
        var returnAttributes = {};
        $.each(attributes, function(key, value) {
            // in format title="value"
            var text = value.substring(0, value.length - 1);
            var keyValuePair = text.split('="');
            returnAttributes[keyValuePair[0]] = keyValuePair[1];
        });
        return returnAttributes;
    }

    function getMarginPadding(element) {
        var marginTop = parseInt($(element).find(".margin-top").attr("data-value"));
        var marginBottom = parseInt($(element).find(".margin-bottom").attr("data-value"));
        var marginLeft = parseInt($(element).find(".margin-left").attr("data-value"));
        var marginRight = parseInt($(element).find(".margin-right").attr("data-value"));
        var paddingTop = parseInt($(element).find(".padding-top").attr("data-value"));
        var paddingBottom = parseInt($(element).find(".padding-bottom").attr("data-value"));
        var paddingLeft = parseInt($(element).find(".padding-left").attr("data-value"));
        var paddingRight = parseInt($(element).find(".padding-right").attr("data-value"));
        //figure out margin classes
        var marginArray = Array();
        var paddingArray = Array();
        // margin top and bottom
        if (marginTop == marginBottom && marginBottom != 0) {
            marginArray.push("margin-y-" + marginTop);
        }
        if (marginTop != marginBottom && marginTop != 0) {
            marginArray.push("margin-top-" + marginTop);
        }
        if (marginTop != marginBottom && marginBottom != 0) {
            marginArray.push("margin-bottom-" + marginBottom);
        }
        // margin left and right
        if (marginLeft == marginRight && marginRight != 0) {
            marginArray.push("margin-x-" + marginLeft);
        }
        if (marginLeft != marginRight && marginLeft != 0) {
            marginArray.push("margin-left-" + marginLeft);
        }
        if (marginLeft != marginRight && marginRight != 0) {
            marginArray.push("margin-right-" + marginRight);
        }
        // all the same
        if (marginTop == marginBottom && marginTop == marginLeft && marginTop == marginRight && paddingTop != 0) {
            marginArray = Array("margin-" + marginTop);
        }
        // padding top and bottom
        if (paddingTop == paddingBottom && paddingBottom != 0) {
            paddingArray.push("padding-y-" + paddingTop);
        }
        if (paddingTop != paddingBottom && paddingTop != 0) {
            paddingArray.push("padding-top-" + paddingTop);
        }
        if (paddingTop != paddingBottom && paddingBottom != 0) {
            paddingArray.push("padding-bottom-" + paddingBottom);
        }
        // padding left and right
        if (paddingLeft == paddingRight && paddingRight != 0) {
            paddingArray.push("padding-x-" + paddingLeft);
        }
        if (paddingLeft != paddingRight && paddingLeft != 0) {
            paddingArray.push("padding-left-" + paddingLeft);
        }
        if (paddingLeft != paddingRight && paddingRight != 0) {
            paddingArray.push("padding-right-" + paddingRight);
        }
        if (paddingTop == paddingBottom && paddingTop == paddingLeft && paddingTop == paddingRight && paddingTop != 0) {
            paddingArray = Array("padding-" + paddingTop);
        }
        // combine them all
        var spacingClasses = marginArray.join(" ") + " " + paddingArray.join(" ");
        return spacingClasses;
    }

    function spacingToArray(value) {
        var values = value.split(" ");
        var returnArray = {};
        $.each(values, function(key, spacingValue) {
            // margin top and bottom
            if (spacingValue.indexOf("margin-top-") != -1) {
                returnArray["margin-top"] = spacingValue.replace("margin-top-", "");
            }
            if (spacingValue.indexOf("margin-bottom-") != -1) {
                returnArray["margin-bottom"] = spacingValue.replace("margin-bottom-", "");
            }
            if (spacingValue.indexOf("margin-y-") != -1) {
                returnArray["margin-top"] = spacingValue.replace("margin-y-", "");
                returnArray["margin-bottom"] = spacingValue.replace("margin-y-", "");
            }
            // margin left and right
            if (spacingValue.indexOf("margin-left-") != -1) {
                returnArray["margin-left"] = spacingValue.replace("margin-left-", "");
            }
            if (spacingValue.indexOf("margin-right-") != -1) {
                returnArray["margin-right"] = spacingValue.replace("margin-right-", "");
            }
            if (spacingValue.indexOf("margin-x-") != -1) {
                returnArray["margin-left"] = spacingValue.replace("margin-x-", "");
                returnArray["margin-right"] = spacingValue.replace("margin-x-", "");
            }
            // padding top and bottom
            if (spacingValue.indexOf("padding-top-") != -1) {
                returnArray["padding-top"] = spacingValue.replace("padding-top-", "");
            }
            if (spacingValue.indexOf("padding-bottom-") != -1) {
                returnArray["padding-bottom"] = spacingValue.replace("padding-bottom-", "");
            }
            if (spacingValue.indexOf("padding-y-") != -1) {
                returnArray["padding-top"] = spacingValue.replace("padding-y-", "");
                returnArray["padding-bottom"] = spacingValue.replace("padding-y-", "");
            }
            // padding left and right
            if (spacingValue.indexOf("padding-left-") != -1) {
                returnArray["padding-left"] = spacingValue.replace("padding-left-", "");
            }
            if (spacingValue.indexOf("padding-right-") != -1) {
                returnArray["padding-right"] = spacingValue.replace("padding-right-", "");
            }
            if (spacingValue.indexOf("padding-x-") != -1) {
                returnArray["padding-left"] = spacingValue.replace("padding-x-", "");
                returnArray["padding-right"] = spacingValue.replace("padding-x-", "");
            }
        });
        return returnArray;
    }

    /*
    * Admin page actions
    */

    $("#rebuild-css").click(function() {
        var button = $(this)
        $(button).addClass("loading");
        var senddata = {
            "action": "rebuildcss"
        }
        $.post(panini_ajax_object.ajax_url, senddata, function() {
            //$(button).removeClass("loading");
        });
    });

    $("#rebuild-icons").click(function() {
        var button = $(this)
        $(button).addClass("loading");
        var senddata = {
            "action": "rebuildicons"
        }
        $.post(panini_ajax_object.ajax_url, senddata, function() {
            $(button).removeClass("loading");
        });
    });

});