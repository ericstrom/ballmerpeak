<?php get_header(); ?>
<section id="content" role="main">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    		<?php if (get_post_meta(get_the_ID(), "has_header", true)): ?>
			<div id="page-header" class="header" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-size: cover;">
				<div class="background <?php echo get_post_meta(get_the_ID(), "header_color", true); ?>" data-text="<?php the_title(); ?>"></div>
				<div class="header-text">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
    		<?php endif; ?>
            <section class="entry-content">
                <?php the_content(); ?>
            </section>
        </article>
    <?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>