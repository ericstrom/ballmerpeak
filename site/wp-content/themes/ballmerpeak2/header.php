<!DOCTYPE html>
<html <?php language_attributes(); ?>>
   <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>" />
      <meta name="viewport" content="width=device-width" />
      <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
      <?php wp_head(); ?>
   </head>
   <body <?php body_class(); ?>>

      <div id="modal">
        <div class="content">
          <p class="message">Are you 21 or older || Can you drink alcohol in your area? || Are you lying about your age?</p>
          <span class="button" id="modal-yes">Yes</span>
          <span class="button" id="modal-no">No</span>
        </div>
      </div>

      <div id="wrapper" class="hfeed">
      <?php
      $class = "";
      if (get_post_meta(get_the_ID(), "has_header", true)) {
        $class = "has-header";
      }
      ?>
      <header id="header" role="banner" class="<?php echo $class; ?>">
        <div class="row">
            <section class="branding">
                <a href="/">
                  <img src="/wp-content/themes/ballmerpeak2/images/logo-mobile.svg" id="mobile-logo" />
                </a>
            </section>
            <nav id="menu" role="navigation" class="cart-count-<?php echo WC()->cart->get_cart_contents_count(); ?>">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
                <div class="cart-wrapper">
                  <a href="<?php echo wc_get_cart_url(); ?>">
                    <i class="fa fa-shopping-cart"></i>
                    <?php echo WC()->cart->get_cart_contents_count(); ?>
                  </a>
                </div>
            </nav>
            <div id="mobile-menu" class="tablet-show">
                <div></div>
                <div></div>
                <div></div>
            </div>
         </div>
      </header>
      <div id="container">