/*
* Global scripts
*/

// header
jQuery(document).ready(function($) {

    // scroll class
    $(window).scroll(function() {
        var scrollThreshold = $("#header").height() + $("#page-header").height();
        if ($(window).scrollTop() > scrollThreshold) {
            $("#header").addClass("scroll");
        } else {
            $("#header").removeClass("scroll");
        }
    });

    // mobile menu
    $("#mobile-menu").click(function() {
        $("body").toggleClass("mobile-menu-expanded");
    });


});