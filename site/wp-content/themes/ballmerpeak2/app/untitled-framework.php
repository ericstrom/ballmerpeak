<?php

/*
* Post meta handling
*/

// empty array
// each post type can add what default meta values should display
// plugins get loaded before themes so any meta thats defined in a plugin
// could potentially create $globalMeta before this line
if (!isset($globalMeta)) {
    $globalMeta = array();
}

// rendering
// Register meta box(es).
function registerPostMetaBox() {
    global $globalMeta, $post;
    if (array_key_exists($post->post_type, $globalMeta)) {
        foreach ($globalMeta[$post->post_type] as $metaInfo) {
            add_meta_box($metaInfo["id"], __( $metaInfo["label"], 'textdomain' ), 'metaBoxCallback', $post->post_type, $metaInfo["context"], $metaInfo["priority"], $metaInfo["fields"]);
        }
    }
    foreach ($globalMeta["all"] as $metaInfo) {
        add_meta_box($metaInfo["id"], __( $metaInfo["label"], 'textdomain' ), 'metaBoxCallback', $post->post_type, $metaInfo["context"], $metaInfo["priority"], $metaInfo["fields"]);
    }
}

add_action( 'add_meta_boxes', 'registerPostMetaBox' );

// rendering
// render the meta boxes on the post edit page
function metaBoxCallback($post, $metaBox) {
    // add nonce
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
    // render the inputs
    // args is the key passed by the callback args in add_meta_box()
    displayMetaInputs($metaBox["args"]);
}

function displayMetaInputs($meta) {
    global $post;
    // loop through meta
    echo "<table>";
    foreach ($meta as $key => $metaInfo) {
        $metaVal = get_post_meta($post->ID, $key, true);
        if (!$metaVal && array_key_exists("default", $metaInfo)) {
            $metaVal = $metaInfo["default"];
        }
        ?>
        </tr>
            <td valign="top">
                <label><?php echo $metaInfo["label"]; ?></label>
            </td>
            <td valign="top">
        <?php
        switch ($metaInfo["type"]) {
            case "text":
                echo '<input type="text" name="' . $key . '" value="' . $metaVal . '" />';
                break;
            case "textarea":
                echo '<textarea name="' . $key . '">' . $metaVal . '</textarea>';
                break;
            case "select":
                echo '<select id="' . $key . '" name="' . $key . '">';
                echo '<option value="">-- Please Select --</option>';
                foreach ($metaInfo["options"] as $label => $option) {
                    $selected = "";
                    if ($metaVal == $option) {
                        $selected = " selected";
                    }
                    echo '<option value="' . $option . '"' . $selected . '>' . $label . '</option>';
                }
                echo '</select>';
                break;
            case "date":
                echo '<input type="text" name="' . $key . '" class="isDatePicker" value="' . $metaVal . '" />';
                break;
            case "upload":
                $image = '';
                if ($metaVal && $metaVal != '') {
                    $image = '<img src="' . $metaVal . '" />';
                }
                echo '<div class="preview-wrapper">' . $image . '</div>';
                echo '<input type="text" name="' . $key . '" id="' . $key . '" class="file-input" value="' . $metaVal . '" onchange="alert("hi");" />';
                echo '<button type="button" class="button meta-add-image insert-media add_media" data-editor="' . $key . '"><i class="fa fa-upload"></i></button>';
                break;
            case "post-select":
                $postCount = wp_count_posts($metaInfo["post_type"]);
                if ($postCount->publish > 150) {
                    echo '<select class="post-search-select" data-post-type="' . $metaInfo["post_type"] . '">';
                    echo '<option>No posts match your search</option>';
                    echo '</select>';
                } else {
                    // there's a small enough ammount of posts that we can display them
                    // in a select element without taking up too many resources
                    $args = array(
                        "post_type"         => $metaInfo["post_type"],
                        "posts_per_page"    => -1
                    );
                    $posts = get_posts($args);
                    echo "<select>";
                    echo '<option value="">-- Please Select --</option>';
                    foreach ($posts as $availablePost) {
                        echo '<option data-text="' . $availablePost->post_title . '" value="' . $availablePost->ID . '">' . $availablePost->post_title . '</option>';
                    }
                    echo "</select>";
                    wp_reset_postdata();
                }
                // add other post select actions
                echo '<i class="action-icon icon-post-add fa fa-plus"></i>';
                echo '<i class="action-icon icon-post-search fa fa-search"></i>';
                echo '<input type="text" class="action-post-search" />';
                echo '<input class="post-list" type="hidden" name="' . $key .'" value="' . $metaVal . '" />';
                echo '<div class="added-post-list">';
                // output the added posts
                if ($metaVal && $metaVal != '' && $metaVal != "[]") {
                    $parsedMetaVal = json_decode($metaVal);
                    $args = array(
                        'post_type'         => $metaInfo["post_type"],
                        'post__in'          => $parsedMetaVal,
                        'orderby'           => 'post__in',
                        'posts_per_page'    => -1
                    );
                    $posts = get_posts($args);
                    foreach ($posts as $selectedPost) {
                        echo '<div class="added-post" data-post-id="' . $selectedPost->ID . '">' . $selectedPost->post_title;
                        echo '<i class="remove-post action-icon fa fa-minus"></i>';
                        echo '</div>';
                    }
                }
                echo '</div>';
                wp_reset_postdata();
                break;
            case "multiple-text":
                echo '<div class="multi-text-wrapper">';
                echo '<input class="item-list" type="hidden" name="' . $key . '" value="' . htmlspecialchars($metaVal) . '" />';
                echo '<input type="text" class="multi-item-input item-input" /><i class="fa fa-plus action-icon item-add">Add ' . $metaInfo["label"] . '</i>';
                echo '<div class="added-item-list">';
                if ($metaVal && $metaVal != '') {
                    $parsedMetaVal = json_decode($metaVal);
                    foreach ($parsedMetaVal as $val) {
                        echo '<div class="added-item" data-item-val="' . $val . '">' . $val;
                        echo '<i class="remove-item action-icon fa fa-minus"></i>';
                        echo '</div>';
                    }
                }
                echo '</div>';
                break;
            case "toggle":
                if ($metaVal == "1") {
                    echo '<span>Yes <input type="radio" name="' . $key .'" checked="checked" value="1">';
                    echo '<span>No <input type="radio" name="' . $key . '"value="0">';
                } else {
                    echo '<span>Yes <input type="radio" name="' . $key .'" value="1">';
                    echo '<span>No <input type="radio" name="' . $key . '" checked="checked" value="0">';
                }
                break;
            case "template":
                $templates = get_page_templates();
                echo '<select name="' . $key . '"">';
                echo '<option value="">Default Template</option>';
                foreach ($templates as $name => $file) {
                    $selected = "";
                    if ($metaVal == $file) {
                        $selected = " selected";
                    }
                    echo '<option value="' . $file .'"' . $selected . '>' . $name . '</option>';
                }
                echo '</select>';
                break;
            default:
                echo '<input type="text" name="' . $key . '" value="' . $metaVal . '" />';
                break;
        }
        ?>
            </td>
        </tr>
        <?php
    }
    echo "</table>";
}

// saving
function saveMetaInputs($post_id, $post, $update) {
    global $globalMeta;

    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    // save all defined meta for this post type
    // NOTE: unlike taco, this doesn't keep post meta limited to these values
    // this is just a handy helper method for defining, displaying and saving
    // meta that we know that we are going to use for this post type
    
    if (array_key_exists($post->post_type, $globalMeta)) {
        foreach ($globalMeta[$post->post_type] as $metaBoxes) {
            foreach ($metaBoxes["fields"] as $key => $value) {
                if (isset($_POST[$key])) {
                    update_post_meta($post_id, $key, $_POST[$key]);
                }
            }
        }
    }
    foreach ($globalMeta["all"] as $metaBoxes) {
        foreach ($metaBoxes["fields"] as $key => $value) {
            if (isset($_POST[$key])) {
                update_post_meta($post_id, $key, $_POST[$key]);
            }
        }
    }
}

add_action("save_post", "saveMetaInputs", 10, 3);
