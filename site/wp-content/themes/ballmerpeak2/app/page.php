<?php


/*
* Post meta for all post types
*/

if (!isset($globalMeta)) {
    $globalMeta = array();
}
$globalMeta["page"] = array(
    array(
        "label"     => "General Settings",
        "id"        => "general-meta-box",
        "context"   => "normal",
        "priority"  => "default",
        "fields"    => array(
            "autop"  => array(
                "type"      => "toggle",
                "label"     => "Remove Auto Paragraph"
            ),
            "custom_css"    => array(
                "type"  => "multiple-text",
                "label" => "Custom Css"
            ),
            "custom_js"    => array(
                "type"  => "multiple-text",
                "label" => "Custom JS"
            ),
            "has_header"	=> array(
            	"type"		=> "toggle",
            	"label"		=> "Show Header Image"
            ),
            "header_color"  => array(
                "type"      => "text",
                "label"     => "Header Background Color"
            )
        )
    )
);


/*
* Toggle auto p according to post meta
*/

function toggleAutoP() {
    $toggleAutoP = get_post_meta(get_queried_object()->ID, "autop", true);
    if ($toggleAutoP == "1") {
        remove_filter( 'the_content', 'wpautop' );
        remove_filter( 'the_excerpt', 'wpautop' );
    }
}

add_action("wp", "toggleAutoP");


/*
* Add any css specified
*/

function customPageStylesScripts() {
    // styles
    $additionalCss = get_post_meta(get_queried_object()->ID, "custom_css", true);
    if ($additionalCss && $additionalCss != "") {
        $cssPaths = json_decode($additionalCss);
        foreach ($cssPaths as $key => $cssPath) {
            wp_enqueue_style("custom-css-" . $key, $cssPath);
        }
    }
    // scripts
    $additionalJs = get_post_meta(get_queried_object()->ID, "custom_js", true);
    if ($additionalJs && $additionalJs != "") {
        $jsPaths = json_decode($additionalJs);
        foreach ($jsPaths as $jsPath) {
            wp_enqueue_script("custom-js-" . $key, $jsPath, array(), 1, true);
        }
    }
}

add_action("wp", "customPageStylesScripts");