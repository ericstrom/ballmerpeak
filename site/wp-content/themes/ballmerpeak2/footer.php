			<div class="clear"></div>
		</div>
		<footer id="footer" role="contentinfo">
			<img src="/wp-content/themes/ballmerpeak2/images/logo-mobile.svg" id="footer-logo" />
			<div class="contact">
				480.205.5637<span>|</span>
				<div class="social-links">
					<a href="https://www.instagram.com/ballmerpeakco/" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="https://www.facebook.com/BallmerPeakDistillery/" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/ballmerpeakco" target="_blank"><i class="fa fa-twitter"></i></a>
					<!-- <a href="" target="_blank"><i class="fa fa-youtube"></i></a> -->
				</div>
			</div>
			<p class="address">
				12347 W Alameda Pkwy, Lakewood CO, 80244
			</p>
		</footer>
	</div>
	<?php wp_footer(); ?>
</body>
</html>