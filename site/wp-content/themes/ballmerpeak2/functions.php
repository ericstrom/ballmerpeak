<?php
add_action( 'after_setup_theme', 'ballmerpeak_setup' );
function ballmerpeak_setup()
{
load_theme_textdomain( 'ballmerpeak2', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'ballmerpeak' ) )
);
}
add_action( 'wp_enqueue_scripts', 'ballmerpeak_load_scripts' );
function ballmerpeak_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'ballmerpeak_enqueue_comment_reply_script' );
function ballmerpeak_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'ballmerpeak_title' );
function ballmerpeak_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'ballmerpeak_filter_wp_title' );
function ballmerpeak_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'ballmerpeak_widgets_init' );
function ballmerpeak_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'ballmerpeak' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function ballmerpeak_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'ballmerpeak_comments_number' );
function ballmerpeak_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

/*
* Custom functions
*/

// framework
include("app/untitled-framework.php");
include("app/page.php");

//get rid of admin bar
add_filter('show_admin_bar', '__return_false');


function bpAddStyles() {
    // lib
    wp_enqueue_style("google-fonts", "https://fonts.googleapis.com/css?family=Merriweather:300,400|Oswald:400,700");
    wp_enqueue_style("font-awesome", "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    // internal
    wp_enqueue_style("bp-base", get_template_directory_uri() . "/css/base.css");
    wp_enqueue_style("bp-header", get_template_directory_uri() . "/css/header.css");
    wp_enqueue_style("bp-footer", get_template_directory_uri() . "/css/footer.css");
    wp_enqueue_style("bp-pages", get_template_directory_uri() . "/css/pages.css");
    wp_enqueue_style("bp-shop", get_template_directory_uri() . "/css/shop.css");
}
add_action("wp_enqueue_scripts", "bpAddStyles");

function bpAddScripts() {
    // lib
    
    // internal
    wp_enqueue_script("bp-general", get_template_directory_uri() . "/js/global.js", array("jquery"));
}
add_action("wp_enqueue_scripts", "bpAddScripts");